import 'dart:math';

import 'package:aa_flutter_app/Config.dart';
import 'package:flutter/material.dart';
import 'package:aa_flutter_app/aa_HomeScreenTour.dart';

import 'package:aa_flutter_app/Login/AdminPage.dart';
import 'package:aa_flutter_app/Login/MemberPage.dart';
import 'package:aa_flutter_app/Config.dart';
import 'package:http/http.dart' as http;

class ItemFromGridDetails extends StatefulWidget {
  final prod_name;

  final prod_pricture;
  final prod_old_price;
  final prod_price;

  @override
  _ItemFromGridDetailsState createState() => _ItemFromGridDetailsState();

  ItemFromGridDetails(
      this.prod_name, this.prod_pricture, this.prod_old_price, this.prod_price);
}

class _ItemFromGridDetailsState extends State<ItemFromGridDetails> {



  Future<String> carousel_fill_book() async
  {
    List data;


    Random rnd;


/*

    int min = 1, max = 5;
    int r1 = min + rnd.nextInt(max - min);
    int r2 = min + rnd.nextInt(max - min);
    */
    print("ORGADDRESS :: " + Config.url_book);
    final response = await http.post(Uri.parse(Config.url_book), body: {


      //final response = await http.post(Uri.parse("http://192.168.0.101/aa_login"), body: {
      "user_id": 1,
      "package_id": 2,
    }, headers: {"Accept": "application/json"});










    return "Success!";

  }

  Future<String> carousel_fill_cart() async
  {
    List data;
    Random rnd;

    /*
    int min = 1, max = 5;
    int r1 = min + rnd.nextInt(max - min);
    int r2 = min + rnd.nextInt(max - min);*/
    print("ORGADDRESS :: " + Config.url_book);
    final response = await http.post(Uri.parse(Config.url_cart), body: {


      //final response = await http.post(Uri.parse("http://192.168.0.101/aa_login"), body: {
      "user_id": 1,
      "package_id": 4,
    }, headers: {"Accept": "application/json"});







    return "Success!";

  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.green,
        title: new Text("Tour App"),
        centerTitle: true,
        elevation: 10.0,
        actions: <Widget>[
          new IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),


        ],
      ),
      body: new ListView(
        children: <Widget>[
          //image carousel begins here

          Container(
            height: 180,
            child: GridTile(
              child: Container(
                  color: Colors.white,
                  child: Image.asset(widget.prod_pricture)),
              footer: new Container(
                color: Colors.white70,
                child: ListTile(
                  leading: new Text(
                    widget.prod_name,
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16.00),
                  ),
                  title: new Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("\$${widget.prod_old_price}",
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w800,
                              decoration
                                  :TextDecoration.lineThrough),
                        ),


                      ),
                      Expanded(
                        child: new Text("\$${widget.prod_price}",
                          style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.w800,
                          ),),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),


          Row(
            children: <Widget>[
              Expanded(
                child: MaterialButton(
                  onPressed: () {

                    carousel_fill_book();
                    Navigator.pushReplacementNamed(context, '/TourHome');

                  },
                  color: Colors.red,
                  textColor: Colors.white,
                  elevation: 2.0,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("Buy Now",textAlign: TextAlign.center,),
                      ),

                    ],
                  ),
                ),
              ),

              Expanded(
                child: MaterialButton(
                  onPressed: () {

                    carousel_fill_cart();
                    Navigator.pushReplacementNamed(context, '/TourHome');
                  },
                  color: Colors.red,
                  textColor: Colors.white,
                  elevation: 2.0,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("Add to Cart",textAlign: TextAlign.center,),
                      ),

                    ],
                  ),
                ),
              ),


            ],
          ),
          Divider(),
          new ListTile(
            title: new Text("Package Details"),
            subtitle: new Text("  Sightseeing Vythiri View Point, Pookode lake, Chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake and Overnight stay at hotel."),
          ),
          Divider(),
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Package Name",style: TextStyle(color: Colors.grey))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text(widget.prod_name,style: TextStyle(color: Colors.grey)))

            ],
          )
          ,
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Price Price",style: TextStyle(color: Colors.grey))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text("\$${widget.prod_price}",style: TextStyle(color: Colors.grey)))


            ],
          ),
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Number of people",style: TextStyle(color: Colors.grey))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text("10",style: TextStyle(color: Colors.grey)))


            ],
          )
        ],
      ),

    );
  }
}
